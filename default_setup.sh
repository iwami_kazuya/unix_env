#!/bin/bash

printf "password: "
read password

SCRIPT_DIR=$(cd $(dirname $0); pwd)

echo "$password" | sudo -S add-apt-repository ppa:pi-rho/dev # For tmux 2.0
echo "$password" | sudo -S apt -y update
echo "$password" | sudo -S apt -y upgrade
echo "$password" | sudo -S apt -y install git zsh tmux cmake build-essential libv4l-dev
echo -e "Host github.com\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

echo "Set my .zshrc and .tmux.conf"
cp $SCRIPT_DIR/zsh/.zshrc $HOME
cp $SCRIPT_DIR/tmux/.tmux.conf $HOME
cp $SCRIPT_DIR/vim/.vimrc $HOME

echo "Make workspace"
WORK_SPACE=$HOME/workspace
if [ -e $WORK_SPACE ]; then
    echo "  $WORK_SPACE found."
else
    mkdir $WORK_SPACE
fi

echo "Change your shell to zsh"
cat /etc/shells
chsh

echo "Generate ssh key"
SSH_KEY_PATH=$HOME/.ssh/id_rsa
if [ -e $SSH_KEY_PATH ]; then
    echo "  $SSH_KEY_PATH found."
else
    ssh-keygen -t rsa
fi

echo "Rename folders from ja to en"
DESKTOP_PATH=$HOME/Desktop
if [ -e $DESKTOP_PATH ]; then
    echo "  No need"
else
    LANG=C xdg-user-dirs-gtk-update
fi

echo "Finish"
