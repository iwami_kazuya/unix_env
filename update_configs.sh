#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

echo "Set my .zshrc and .tmux.conf"
cp $SCRIPT_DIR/zsh/.zshrc $HOME
cp $SCRIPT_DIR/tmux/.tmux.conf $HOME
cp $SCRIPT_DIR/vim/.vimrc $HOME
