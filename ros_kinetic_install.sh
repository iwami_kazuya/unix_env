#!/bin/bash

printf "password: "
read password

SCRIPT_DIR=$(cd $(dirname $0); pwd)

echo "$password" | sudo -S sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
echo "$password" | sudo -S apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
echo "$password" | sudo -S apt -y update
echo "$password" | sudo -S apt -y install ros-kinetic-desktop-full
echo "$password" | sudo -S rosdep init
rosdep update
echo "source /opt/ros/kinetic/setup.zsh" >> ~/.zshrc
source ~/.zshrc

echo "$password" | sudo -S apt -y install python-rosinstall python-rosinstall-generator python-wstool build-essential -y
echo "$password" | sudo -S apt -y install python-catkin-tools -y
echo "$password" | sudo -S apt -y install ros-kinetic-geometry -y

mkdir -p ~/catkin_ws/src
cd ~/catkin_ws
catkin init
catkin config --extend /opt/ros/kinetic
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin config --merge-devel # this is important, otherwise you may get weird linking errors
cd src
wstool init
wstool merge $SCRIPT_DIR/ros/ethz_asl.rosinstall
wstool update -j4
echo "source ~/catkin_ws/devel/setup.zsh" >> ~/.zshrc
source ~/.zshrc
